import pc.model.Einheit;
import pc.model.Truppe;

public class BattleSim {

	public static void main(String[] args) {
		Einheit miliz = new Einheit("Miliz", 15, 5, 0.8, 100, false, true, false, false, false, 1);
		Einheit rekrut = new Einheit("Rekrut", 40, 15, 0.8, 43, false, true, false, false, false, 2);
		Einheit ritter = new Einheit("Ritter", 90, 20, 0.8, 100, false, true, false, false, false, 3);
		Einheit kuerassier = new Einheit("K�rassier", 120, 15, 0.8, 20, true, false, false, true, false, 4);
		Einheit kavallerie = new Einheit("Kavallerie", 5, 5, 0.8, 47, true, false, false, true, false, 5);
		Einheit bogenschuetze = new Einheit("Bogensch�tze", 10, 20, 0.8, 10, false, true, false, false, false, 6);
		Einheit langbogenschuetze = new Einheit("Langbogensch�tze", 10, 20, 0.8, 10, true, false, true, false, false, 7);
		Einheit armbrustschuetze = new Einheit("Armbrustsch�tze", 10, 110, 0.8, 10, false, false, true, false, false, 8);
		Einheit kanonier = new Einheit("Kanonier", 60, 80, 0.8, 10, false, false, true, false, true, 9);
		
		
		
		Einheit orkPluenderer = new Einheit("Orc-Pl�nderer", 40, 15, 0.6, 134, false, true, false, false, false, 1);
		Einheit wargenreiter = new Einheit("Wargenreiter", 5, 5, 0.6, 276, true, false, false, false, false, 2);
		Einheit orkJaeger = new Einheit("Ork-J�ger", 10, 20, 0.6, 140, false, true, false, false, false, 3);
		Einheit orkEliteJaeger = new Einheit("Elite-Ork-J�ger", 10, 20, 0.6, 140, true, false, true, false, false, 4);

		Truppe eigeneTruppe = new Truppe();
		//eigeneTruppe.hinzufuegen(miliz);
		eigeneTruppe.hinzufuegen(bogenschuetze);
		eigeneTruppe.hinzufuegen(rekrut);
		eigeneTruppe.hinzufuegen(kavallerie);

		Truppe gegnerischeTruppe = new Truppe();
		gegnerischeTruppe.hinzufuegen(orkJaeger);

		System.out.println(eigeneTruppe);
		System.out.println(gegnerischeTruppe);
		System.out.println("---k�mpfen---");

		// K�mpfen
		do {
			gegnerischeTruppe.schadenZufuegen(eigeneTruppe.getErstschlagSchaden(),1);
			eigeneTruppe.schadenZufuegen(gegnerischeTruppe.getErstschlagSchaden(),1);
			
			gegnerischeTruppe.schadenZufuegen(eigeneTruppe.getHauptschlagSchaden(),2);
			eigeneTruppe.schadenZufuegen(gegnerischeTruppe.getHauptschlagSchaden(),2);

		} while (eigeneTruppe.getTruppenLeben() > 0 && gegnerischeTruppe.getTruppenLeben() > 0);

		System.out.println(eigeneTruppe);
		System.out.println(gegnerischeTruppe);

	}

}
