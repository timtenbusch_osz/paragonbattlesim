package pc.model;

public class Einheit {

	private String name;
	private int leben;
	private int schaden;
	private double trefferwahrscheinlichkeit;
	private int anzahl;
	private boolean hatErstschlag;
	private boolean hatAngriff;
	private boolean hatLetztenSchlag;
	private boolean flankieren;
	private boolean bereichsschaden;
	private int reihenfolge;
	
	public Einheit(String name, int leben, int schaden, double trefferwahrscheinlichkeit, int anzahl,
			boolean hatErstschlag, boolean hatAngriff, boolean hatLetztenSchlag, boolean flankieren, boolean bereichsschaden, int reihenfolge) {
		super();
		this.name = name;
		this.leben = leben;
		this.schaden = schaden;
		this.trefferwahrscheinlichkeit = trefferwahrscheinlichkeit;
		this.anzahl = anzahl;
		this.hatErstschlag = hatErstschlag;
		this.hatAngriff = hatAngriff;
		this.hatLetztenSchlag = hatLetztenSchlag;
		this.setBereichsschaden(bereichsschaden);
		this.setFlankieren(flankieren);
		this.setReihenfolge(reihenfolge);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLeben() {
		return leben;
	}

	public void setLeben(int leben) {
		this.leben = leben;
	}

	public int getSchaden() {
		return schaden;
	}

	public void setSchaden(int schaden) {
		this.schaden = schaden;
	}

	public double getTrefferwahrscheinlichkeit() {
		return trefferwahrscheinlichkeit;
	}

	public void setTrefferwahrscheinlichkeit(double trefferwahrscheinlichkeit) {
		this.trefferwahrscheinlichkeit = trefferwahrscheinlichkeit;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public boolean isHatErstschlag() {
		return hatErstschlag;
	}

	public void setHatErstschlag(boolean hatErstschlag) {
		this.hatErstschlag = hatErstschlag;
	}

	public boolean isHatAngriff() {
		return hatAngriff;
	}

	public void setHatAngriff(boolean hatAngriff) {
		this.hatAngriff = hatAngriff;
	}

	public boolean isHatLetztenSchlag() {
		return hatLetztenSchlag;
	}

	public void setHatLetztenSchlag(boolean hatLetztenSchlag) {
		this.hatLetztenSchlag = hatLetztenSchlag;
	}

	public boolean isFlankieren() {
		return flankieren;
	}

	public void setFlankieren(boolean flankieren) {
		this.flankieren = flankieren;
	}

	public boolean isBereichsschaden() {
		return bereichsschaden;
	}

	public void setBereichsschaden(boolean bereichsschaden) {
		this.bereichsschaden = bereichsschaden;
	}

	public int getReihenfolge() {
		return reihenfolge;
	}

	public void setReihenfolge(int reihenfolge) {
		this.reihenfolge = reihenfolge;
	}

	@Override
	public String toString() {
		return  anzahl + "\t" + name;
	}
	
	
	
	
	
}
