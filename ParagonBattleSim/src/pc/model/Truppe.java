package pc.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Truppe {

	private List<Einheit> truppe;
	private List<Einheit> verluste;
	public final int MAXANZAHLEINHEITEN = 100;

	public Truppe() {
		this.truppe = new ArrayList<Einheit>(MAXANZAHLEINHEITEN);
		this.verluste = new ArrayList<Einheit>(MAXANZAHLEINHEITEN);
	}

	public List<Einheit> getTruppe() {
		return truppe;
	}

	public void setTruppe(List<Einheit> truppe) {
		this.truppe = truppe;
	}

	public List<Einheit> getVerluste() {
		return verluste;
	}

	public void setVerluste(List<Einheit> verluste) {
		this.verluste = verluste;
	}

	public void hinzufuegen(Einheit einheit) {
		truppe.add(einheit);
	}

	public int getAngriff() {
		return 0;
	}

	@Override
	public String toString() {
		String einheitenAlsText = "";
		for (Einheit e : truppe) {
			einheitenAlsText += e + "\n";
		}
		return einheitenAlsText;
	}

	public int getHauptschlagSchaden() {
		double schaden = 0;
		for (Einheit e : truppe) {
			if (e.isHatAngriff()) {
				schaden += e.getAnzahl() * e.getSchaden() * e.getTrefferwahrscheinlichkeit();
			}
		}
		return (int) Math.round(schaden);
	}

	public int getTruppenLeben() {
		int leben = 0;
		for (Einheit e : truppe) {
			leben += e.getAnzahl() * e.getLeben();
		}
		return leben;
	}

	public void schadenZufuegen(int eigenerSchaden, int typ) {
		List<Einheit> tmp = new ArrayList<Einheit>();
		switch (typ) {
		case 1:
			Collections.sort(this.truppe, new Comparator<Einheit>(){

				@Override
				public int compare(Einheit a, Einheit b) {
					return a.getReihenfolge() - b.getReihenfolge();
				}
				
			});
			break;
		case 2:
			//sortiere Truppe nach Nahkampfreihenfolge
			break;
		}
		for (Einheit e : truppe) {
			int truppenleben = e.getLeben() * e.getAnzahl();
			if (truppenleben > eigenerSchaden) {
				int anzahlTote = eigenerSchaden / e.getLeben();
				e.setAnzahl(e.getAnzahl() - anzahlTote);
				verluste.add(new Einheit(e.getName(), e.getLeben(), e.getSchaden(), e.getTrefferwahrscheinlichkeit(),
						anzahlTote, e.isHatErstschlag(), e.isHatAngriff(), e.isHatLetztenSchlag(), e.isFlankieren(), e.isBereichsschaden(), e.getReihenfolge()));
				return;
			} else {
				verluste.add(e);
				tmp.add(e);
				eigenerSchaden -= truppenleben;
			}
		}
		
		for (Einheit e : tmp)
			truppe.remove(e);
	}

	public int getErstschlagSchaden() {
		double schaden = 0;
		for (Einheit e : truppe) {
			if (e.isHatErstschlag()) {
				schaden += e.getAnzahl() * e.getSchaden() * e.getTrefferwahrscheinlichkeit();
			}
		}
		return (int) Math.round(schaden);
	}
}
